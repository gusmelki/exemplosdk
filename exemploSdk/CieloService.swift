//
//  CieloService.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 24/10/18.
//

import Foundation
import Moya


enum CieloService {
  case getMassiva(ic: String)
  case getHelpCenterInfos
}

// MARK: - TargetType Protocol Implementation
extension CieloService: TargetType {
  var baseURL: URL { return URL(string: "https://apihom.cielo.com.br")! }
  var path: String {
    switch self {
    case .getMassiva:
      return "/appcielo/v1/user/login/status/v2"
    case .getHelpCenterInfos:
      return "/appcielo/v1/help"
    }
  }
  var method: Moya.Method {
    switch self {
    case .getMassiva, .getHelpCenterInfos:
      return .get
    }
  }
  var task: Task {
    switch self {
    case .getMassiva, .getHelpCenterInfos:
      return .requestPlain
    }
  }
  var sampleData: Data {
    switch self {
    case .getMassiva, .getHelpCenterInfos:
      return "Half measures are as bad as nothing at all.".utf8Encoded
    }
  }
  var headers: [String: String]? {
    switch self {
    case .getMassiva(let ic):
      return [
        "Content-Type": "application/json",
        "Accept": "*/*",
        "os": UIDevice.current.systemVersion,
        "channel": "IOS",
        "appVersion": Bundle.main.releaseVersionNumber!,
        "deviceId": Bundle.main.deviceId!,
        "token": "4624E9B0-AWXB-11E3-A5E2-0800200C9A66",
        "deviceModel": UIDevice.current.model,
        "merchantId" : ic
      ]
    case .getHelpCenterInfos:
      return [
        "Content-Type": "application/json",
        "Accept": "*/*",
        "os": UIDevice.current.systemVersion,
        "channel": "IOS",
        "appVersion": Bundle.main.releaseVersionNumber!,
        "deviceId": Bundle.main.deviceId!,
        "token": "4624E9B0-AWXB-11E3-A5E2-0800200C9A66",
        "deviceModel": UIDevice.current.model,
      ]
    }
  }
}
// MARK: - Helpers
private extension String {
  var urlEscaped: String {
    return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
  }
  
  var utf8Encoded: Data {
    return data(using: .utf8)!
  }
}

public extension Bundle {
  var releaseVersionNumber: String? {
    return infoDictionary?["CFBundleShortVersionString"] as? String
  }
  var buildVersionNumber: String? {
    return infoDictionary?["CFBundleVersion"] as? String
  }
  
  var appVersionCode: String? {
    return infoDictionary?["MCAppVersionID"] as? String
  }
  
  var deviceId : String? {
    return UIDevice.current.identifierForVendor?.uuidString
  }
  
  static func loadView<T>(fromNib name: String, withType type: T.Type) -> T {
    if let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T {
      return view
    }
    fatalError("Could not load view with type " + String(describing: type))
  }
}

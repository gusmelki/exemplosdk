//
//  Manager.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 30/10/18.
//

import Foundation

public struct Manager: Codable {
  
  public let name: String?
  public let email: String?
  public let phone: String?
  
  enum CodingKeys: String, CodingKey {
    case name
    case email
    case phone
  }
  
}

//
//  Merchant.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 30/10/18.
//

import Foundation

public struct Merchant: Codable {
  
  public let title: String?
  public let desc: String?
  public let value: String?
  
  enum CodingKeys: String, CodingKey {
    case title
    case desc = "description"
    case value
  }
  
}

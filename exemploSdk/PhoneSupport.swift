//
//  PhoneSupport.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 30/10/18.
//

import Foundation

public struct PhoneSupport: Codable {
  
  public let title: String?
  public let desc: String?
  public let timeDescription: String?
  public let phones: [Phone]?
  
  
  enum CodingKeys: String, CodingKey {
    case title
    case desc = "description"
    case timeDescription = "timeDescription"
    case phones
  }
  
}

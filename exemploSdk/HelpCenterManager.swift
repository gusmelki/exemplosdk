//
//  HelpCenterManager.swift
//  Alamofire
//
//  Created by Gustavo Melki Leal on 30/10/18.
//

import Foundation
import Moya
import RxSwift

public class HelpCenterManager {
  
  static let provider = MoyaProvider<CieloService>()
  
  public static func getHelpCenterInfos() -> Observable<HelpCenter> {
    return self.provider.rx.request(.getHelpCenterInfos)
      .asObservable()
      .filterSuccessfulStatusCodes()
      .map(HelpCenter.self)
  }
}

//
//  Phone.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 30/10/18.
//

import Foundation

public struct Phone: Codable {
  
  public let desc: String?
  public let value: String?
  
  enum CodingKeys: String, CodingKey {
    case desc = "description"
    case value
  }
  
}

//
//  Massiva.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 24/10/18.
//

import Foundation

public struct Massiva: Codable {
  
  public let code: Int?
  public let message: String?
  
  enum CodingKeys: String, CodingKey {
    case code
    case message
  }
  
}

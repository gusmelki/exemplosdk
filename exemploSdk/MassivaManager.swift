//
//  MassivaManager.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 24/10/18.
//

import Foundation
import Moya
import RxSwift

public class MassivaManager {
  
  static let provider = MoyaProvider<CieloService>()
  
  public static func getMassiva(ic : String) -> Observable<Massiva>{
    return self.provider.rx.request(.getMassiva(ic: ic))
      .asObservable()
      .filterSuccessfulStatusCodes()
      .map(Massiva.self)
  }
}

#
# Be sure to run `pod lib lint exemploSdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'exemploSdk'
  s.version          = '0.1.0'
  s.summary          = 'SDK exemplo para resquisições da Cielo.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = 'Descrição : SDK exemplo para resquisições da Cielo.'

  s.homepage         = 'https://gitlab.com/gusmelki/exemplosdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'gusmelki' => 'gusmelki@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/gusmelki/exemplosdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.platform = :ios
  s.ios.deployment_target = '12.0'
  s.requires_arc = true
  s.swift_version = "4.2"


  s.source_files = 'exemploSdk/**/*.{swift}'
  
  # s.resource_bundles = {
  #   'exemploSdk' => ['exemploSdk/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end

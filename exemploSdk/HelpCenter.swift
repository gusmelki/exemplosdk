//
//  HelpCenter.swift
//  exemploSdk
//
//  Created by Gustavo Melki Leal on 30/10/18.
//

import Foundation

public struct HelpCenter: Codable {
  
  public let manager: Manager?
  public let merchant: Merchant?
  public let user: ForgotUser?
  public let pwd: ForgotPassword?
  public let faq: Faq?
  public let technicalQuestions: TechnicalQuestions?
  public let onlineConsultant: OnlineConsultant?
  public let technicalSupport: TechnicalSupport?
  public let phonesSupport: [PhoneSupport]?
  
  enum CodingKeys: String, CodingKey {
    case manager
    case merchant
    case user
    case pwd
    case faq
    case technicalQuestions
    case onlineConsultant
    case technicalSupport
    case phonesSupport
  }
  
}

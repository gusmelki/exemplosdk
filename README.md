# exemploSdk

[![CI Status](https://img.shields.io/travis/gusmelki/exemploSdk.svg?style=flat)](https://travis-ci.org/gusmelki/exemploSdk)
[![Version](https://img.shields.io/cocoapods/v/exemploSdk.svg?style=flat)](https://cocoapods.org/pods/exemploSdk)
[![License](https://img.shields.io/cocoapods/l/exemploSdk.svg?style=flat)](https://cocoapods.org/pods/exemploSdk)
[![Platform](https://img.shields.io/cocoapods/p/exemploSdk.svg?style=flat)](https://cocoapods.org/pods/exemploSdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

exemploSdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'exemploSdk'
```

## Author

gusmelki, gusmelki@gmail.com

## License

exemploSdk is available under the MIT license. See the LICENSE file for more info.

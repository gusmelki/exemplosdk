//
//  ViewController.swift
//  exemploSdk
//
//  Created by gusmelki on 10/24/2018.
//  Copyright (c) 2018 gusmelki. All rights reserved.
//

import UIKit
import exemploSdk
import RxSwift


class ViewController: UIViewController {

  let disposeBag = DisposeBag()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
//      MassivaManager.getMassiva(ic: "2000463023").subscribe(onNext: { (result) in
//
//       print(result)
//
//      }, onError: { (error) in
//        print(error)
//      }, onCompleted: {
//        print("onCompleted")
//      }).disposed(by: disposeBag)
//    }
      
      HelpCenterManager.getHelpCenterInfos().subscribe(onNext: { (result) in
        
        print(result)
        
      }, onError: { (error) in
        print(error)
      }, onCompleted: {
        print("onCompleted")
      }).disposed(by: disposeBag)
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

